# simple-site

Simple Site for retrieve calc-api

## To run in local
Activate environment file first

`virtualenv env`

then

`source env/bin/activate`

To run the in local:

`python3 -m flask run`
