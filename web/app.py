from flask import request, Flask
import requests
import json

app = Flask(__name__)

@app.route('/', methods=['GET'])
def plus_operator():
    if len(request.args) == 0:
        return """
        <form action="/" method="GET">
            <label for="a">a : </label>
            <input type="text" id="a" name="a"><br>
            <label for="b">b : </label>
            <input type="text" id="b" name="b"><br>
            <input type="submit" value="Submit">
        </form>
        """
    else:
        a = int(request.args.get('a'))
        b = int(request.args.get('b'))
        result = requests.post('https://law-simple-api.herokuapp.com/',data={'a':a,'b':b}).json()['hasil']
        return "<html>Hasil: {}</html>".format(result)
